/* eslint "strict": [2, "function"] */
/* eslint-disable space-unary-ops */
//http://jsfiddle.net/brendanowen/uXbn6/8/

angular.module('myApp')

.directive('collapsableTree', ['RecursionHelper', function (RecursionHelper) {
  'use strict';



  return {
    restrict: 'E',
    replace: true,
    scope: {
      data: '='
    },
    // link: function ($scope, $element, attrs) {
    //   $scope.collapsed = false;
    //   $scope.toggle = function () {
    //     $scope.collapsed = !$scope.collapsed;
    //   };
    // },
    templateUrl: 'components/collapsableTree/collapsable-tree.html',
    compile: function(element) {
        return RecursionHelper.compile(element, function(scope, iElement, iAttrs, controller, transcludeFn){
          scope.collapsedMembers = [];
          scope.initiallyCollapsed = false;

          /*
          * Handle multiple same-level collapsable elements (should help reducing number of scopes performance-wise)
          * index- same-level element index, starting from 0, taken from ng-repeat
          */
          scope.toggle = function (index) {
            var elementCollapsed = scope.collapsedMembers[index];

            // if element collapsed for the first time, fill its array position with bollean
            if (typeof(elementCollapsed) !== 'boolean') {
              scope.collapsedMembers[index] = !scope.initiallyCollapsed;
            } else {
              scope.collapsedMembers[index] = !elementCollapsed;
            }
          };

          scope.isCollapsed = function (index) {
            var elementCollapsed = scope.collapsedMembers[index];

            // if element not registered, use default value
            if (typeof(elementCollapsed) !== 'boolean') {
              elementCollapsed = scope.initiallyCollapsed;
              scope.collapsedMembers[index] = elementCollapsed;
            }
            return elementCollapsed;
          };
        });
    }
  };
}]);
