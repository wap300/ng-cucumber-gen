'use strict';

angular.module('myApp.generateStepsView', ['ngRoute'])

.config(['$routeProvider', function ($routeProvider) {
      $routeProvider.when('/generateStepsView', {
        templateUrl: 'generateStepsView/generateStepsView.html',
        controller: 'generateStepsViewCtrl',
        controllerAs: 'generate'
      });
    }
  ])

.controller('generateStepsViewCtrl', [function () {


    this.data1 = [{'title': 'top', 'items': [{
        'id': 1,
        'title': 'dragon-breath',
        'items': []
      }, {
          'id': 2,
          'title': 'cat-breath',
          'items': []
        }
      ]}, {
        'title': 'middle',
        'items': [{
            'id': 1,
            'title': 'dragon-breath',
            'items': [{
                'id': 3,
                'title': 'underneath',
                'items': [{
                    'id': 1,
                    'title': 'dragon-breath',
                    'items': [{
                        'id': 3,
                        'title': 'underneath',
                        'items': []
                      }, {
                          'id': 4,
                          'title': 'bad-breath',
                          'items': []
                    }]
                  }, {
                      'id': 2,
                      'title': 'cat-breath',
                      'items': []
                }]
              }, {
                  'id': 4,
                  'title': 'bad-breath',
                  'items': [{
                      'id': 1,
                      'title': 'dragon-breath',
                      'items': [{
                          'id': 3,
                          'title': 'underneath',
                          'items': []
                        }, {
                            'id': 4,
                            'title': 'bad-breath',
                            'items': []
                      }]
                    }, {
                        'id': 2,
                        'title': 'cat-breath',
                        'items': []
                  }]
            }]
          }, {
              'id': 2,
              'title': 'cat-breath',
              'items': []
        }]
      }, {
        'title': 'bottom',
        'items': [{
            'id': 1,
            'title': 'dragon-breath',
            'items': []
          }, {
              'id': 2,
              'title': 'cat-breath',
              'items': [{
                  'id': 1,
                  'title': 'dragon-breath',
                  'items': [{
                      'id': 3,
                      'title': 'underneath',
                      'items': []
                    }, {
                        'id': 4,
                        'title': 'bad-breath',
                        'items': []
                  }]
                }, {
                    'id': 2,
                    'title': 'cat-breath',
                    'items': []
              }]
        }]
      }];

  this.data = [{
      'id': 0,
      'title': 'top',
      'items': [{
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 4,
          'title': '4. romantic-transclusion',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }, {
          'id': 1,
          'title': '1. dragon-breath',
          'items': []
        }, {
          'id': 2,
          'title': '2. moiré-vision',
          'items': [{
              'id': 21,
              'title': '2.1. tofu-animation',
              'items': [{
                  'id': 211,
                  'title': '2.1.1. spooky-giraffe',
                  'items': []
                }, {
                  'id': 212,
                  'title': '2.1.2. bubble-burst',
                  'items': []
                }
              ]
            }, {
              'id': 22,
              'title': '2.2. barehand-atomsplitting',
              'items': []
            }
          ]
        }, {
          'id': 3,
          'title': '3. unicorn-zapper',
          'items': []
        }
      ]
    }, {

          'id': 0,
          'title': 'bottom',
          'items': [{
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 4,
              'title': '4. romantic-transclusion',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }, {
              'id': 1,
              'title': '1. dragon-breath',
              'items': []
            }, {
              'id': 2,
              'title': '2. moiré-vision',
              'items': [{
                  'id': 21,
                  'title': '2.1. tofu-animation',
                  'items': [{
                      'id': 211,
                      'title': '2.1.1. spooky-giraffe',
                      'items': []
                    }, {
                      'id': 212,
                      'title': '2.1.2. bubble-burst',
                      'items': []
                    }
                  ]
                }, {
                  'id': 22,
                  'title': '2.2. barehand-atomsplitting',
                  'items': []
                }
              ]
            }, {
              'id': 3,
              'title': '3. unicorn-zapper',
              'items': []
            }
          ]
        }
      ];
    }
  ]);
