'use strict';

angular.module('myApp.buildFeatureView', ['ngRoute', 'ui.tree'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/buildFeatureView', {
    templateUrl: 'buildFeatureView/buildFeatureView.html',
    controller: 'buildFeatureViewCtrl',
    controllerAs: 'build'
  });
}])

.controller('buildFeatureViewCtrl', ['$scope', function($scope) {
  this.toggle = function (scope) {
    scope.toggle();
  };

this.data = [
  {
    'id': 0,
    'title': 'top',
    'items': [
      {
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },
      {
        'id': 4,
        'title': '4. romantic-transclusion',
        'items': []
      },
      {
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      },{
        'id': 1,
        'title': '1. dragon-breath',
        'items': []
      },
      {
        'id': 2,
        'title': '2. moiré-vision',
        'items': [
          {
            'id': 21,
            'title': '2.1. tofu-animation',
            'items': [
              {
                'id': 211,
                'title': '2.1.1. spooky-giraffe',
                'items': []
              },
              {
                'id': 212,
                'title': '2.1.2. bubble-burst',
                'items': []
              }
            ]
          },
          {
            'id': 22,
            'title': '2.2. barehand-atomsplitting',
            'items': []
          }
        ]
      },
      {
        'id': 3,
        'title': '3. unicorn-zapper',
        'items': []
      }
    ]}
    ];

    this.data1 = [{
          'id': 1,
          'title': 'node1',
          'nodes': [
            {
              'id': 11,
              'title': 'node1.1',
              'nodes': [
                {
                  'id': 111,
                  'title': 'node1.1.1',
                  'nodes': []
                }
              ]
            },
            {
              'id': 12,
              'title': 'node1.2',
              'nodes': []
            }
          ]
        }, {
          'id': 2,
          'title': 'node2',
          'nodes': [
            {
              'id': 21,
              'title': 'node2.1',
              'nodes': []
            },
            {
              'id': 22,
              'title': 'node2.2',
              'nodes': []
            }
          ]
        }, {
          'id': 3,
          'title': 'node3',
          'nodes': [
            {
              'id': 31,
              'title': 'node3.1',
              'nodes': []
            }
          ]
        }
      ];
}]);
