/*global browser*/
'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function() {


  it('should automatically redirect to /buildFeatureView when location hash/fragment is empty', function() {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch('/buildFeatureView');
  });


  describe('buildFeatureView', function() {

    beforeEach(function() {
      browser.get('index.html#/buildFeatureView');
    });


    it('should render buildFeatureView when user navigates to /buildFeatureView', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/partial for view 1/);
    });

  });


  describe('generateStepsView', function() {

    beforeEach(function() {
      browser.get('index.html#/generateStepsView');
    });


    it('should render generateStepsView when user navigates to /generateStepsView', function() {
      expect(element.all(by.css('[ng-view] p')).first().getText()).
        toMatch(/partial for view 2/);
    });

  });
});
